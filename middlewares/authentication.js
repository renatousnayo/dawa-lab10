const jwt = require("jsonwebtoken");

let verificaToken = (req, res, next) => {
    console.log("++++++++++++++++++verificaToken++++++++++++++++++++");
    //let token = req.get("token"); // leoric
    let token = req.headers['token'];
    console.log("TOKEN RECIBIDO: " + token);

    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err,
            });
        }

        req.usuario = decoded.usuario;

        
        console.log(req.usuario);
        console.log(process.env.SEED);
        
        next();
    });
    console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++");
};

module.exports = {
    verificaToken,
};